﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_objects_properties
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student
            {
                Name = "A1"
            };

            string test = st.Test;
        }
    }
}
