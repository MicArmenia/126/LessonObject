﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_objects_properties
{
    class Student
    {
        public string Name { get; set; }

        private DateTime birthday;
        public DateTime Birthday
        {
            get => birthday;
            set
            {
                Age = DateTime.Now.Year - value.Year;
                if (Age < 15 || Age > 50)
                {
                    Age = 0;
                    birthday = new DateTime();
                }
                else
                    Birthday = value;
            }
        }

        public int Age { get; private set; }

        public string Test { get; private set; }

        public void MyTestMethod()
        {
            Test = "barev....";
        }
    }
}
