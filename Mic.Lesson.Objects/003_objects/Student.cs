﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_objects
{
    class Student
    {
        public Student(string name = null, string surname = null, string email = null, int age = 0)
        {
            this.name = name;
            this.surname = surname;
            this.email = email;
            this.age = age;
        }

        public string name;
        public string surname;
        public string email;
        public int age;
    }
}
