﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_objects
{
    class Program
    {
        static void Main(string[] args)
        {
            Student st = new Student();

            Type type = st.GetType();
            string str = st.ToString();

            int a = 10;
            Console.WriteLine(a);
            Console.WriteLine(st);

            int code = st.GetHashCode();

            Student st1 = new Student();
            Student st2 = new Student();

            if (st1.Equals(st2))
            {

            }

            Student st3 = st1;

            Console.ReadLine();
        }
    }
}
