﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_objects_properties
{
    class Program
    {
        static void Main(string[] args)
        {
            Student st = new Student
            {
                name = "A1",
                surname = "A1yan",
                Age = 250
            };

            //st.SetAge(20);
            //byte age = st.GetAge();

            byte age = st.Age;
        }
    }
}
