﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_objects_properties
{
    class Student
    {
        public string name;
        public string surname;

        private byte age;
        public byte Age
        {
            get { return age; }
            set
            {
                if (value < 17 || value > 50)
                    age = 0;
                else
                    age = value;
            }
        }

        //public byte GetAge()
        //{
        //    return age;
        //}

        //public void SetAge(byte value)
        //{
        //    if (value < 17 || value > 50)
        //        age = 0;
        //    else
        //        age = value;
        //}
    }
}
