﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_objects_properties
{
    class Student
    {
        public string surname;

        //private string name;
        //public string Name
        //{
        //    get { return name; }
        //    set { name = value; }
        //}
        public string Name { get; set; }

        private byte age;
        public byte Age
        {
            get { return age; }
            set
            {
                if (value < 17 || value > 50)
                    age = 0;
                else
                    age = value;
            }
        }
    }
}
