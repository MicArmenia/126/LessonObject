﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_objects_properties
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> studens = CreateStudents(20);

            foreach (Student st in studens)
            {
                //Console.WriteLine($"{st.name} {st.surname} Age: {st.Age}");
                Console.WriteLine($"{st.FullName} Age: {st.Age}");
            }

            Console.ReadLine();
        }

        static List<Student> CreateStudents(int count)
        {
            var items = new List<Student>(count);
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                string name = $"A{i + 1}";
                string surname = name + "yan";
                var st = new Student($"{name}.{surname}@gmail.com")
                {
                    name = name,
                    surname = surname,
                    Age = (byte)rnd.Next(0, 256)
                };

                items.Add(st);
            }

            return items;
        }
    }
}
